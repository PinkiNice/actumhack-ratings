class Role {
  constructor({ name, hsc, ssc }) {
    if (hsc * ssc !== 1) {
      throw new Error("Hard Skill Coefficient * Soft skill Coefficient must be equal to 1");
    }

    this.name = name;
    this.hsc = hsc;
    this.ssc = ssc;
  }
}

class Task {
  constructor({ projects, description, specialization }) {
    this.projects = projects;
    this.description = description;
    this.specialization = specialization;
  }
}

class Hackathon {
  constructor({ tasks }) {
    this.tasks = tasks;
  }
}

class Project {
  /*
    @Param: team - object describing team which made this concrete project.
      It is immutable and has primary key to team to identify it later in time, but
      members inside object are taken once and can not be changed after project is over.

    @Param: source - some information about code itself 
      (for percentage of used programming languages for ex.)

    @Param: place - what place project took on hackathon in it's nomination

    @Param: participations - array of all players, who are authors of these project. 
      (Because teams may change in time)

    @Param: tags - related to project skills (Java, Machine Learning, Web-Design)
      Those skills will be approved for participations

    @Param: task - description of the task, may contain some biases about rating grows.
      for example - gained ratings for designer in Blockchain task is reduced.
  */
  constructor({ team, source, place, task, brainstormMap, effortPartition }) {
    this.team = team;
    this.source = source;
    this.place = place;
    this.task = task;
    this.hackathon = task.hackathon;
    this.brainstormMap = brainstormMap || null;
    this.effortPartition = effortPartition;
    this.tags = tags;
    this.participations = Array.from(team.members);
  }
}

class Participation {
  /*
   @Param: hackathon: reference some particular hack taked place some time ago.
   @Param: project: references project which has been submitted to jury by the team user was in.
   @Param: tags: project required skills in which this person participated for ex. ["Java", "Back-end"]
   @Param: team: under what 'label' Participation taked place in hackathon
  */
  constructor({ user, hackathon, team, project, tags, roles,rolesProjectImpact }) {
    if (roles.length >= 2) {
      // After tags division user can take any roles 
      // throw new Error('Participation can\'t have more than 2 roles in one project');
    }

    this.user = user;
    this.hackathon = hackathon;
    this.project = project;
    this.roles = roles;
    this.rolesProjectImpact = rolesProjectImpact;
    /*
      { tag: 'java', value: 0.3 }, { tag: 'ML', value: 1 }
      Sum of values of each tag must be equal to 1
      (the more of the tag you take the less you leave for your teammates)
    */
    this.tags = tags;
    this.team = team;
  }
}

class User {
  /*
    User info abstracted from hackathon participating.
  */
  constructor({ username, common, socials, roles }) {
    this.username = username;
    this.common = common || {};
    this.socials = socials || {};
    // This is not used in rating, but just to help user find team
    this.roles = roles || [];
  }
}

class Team {
  constructor({ name, members, projects }) {
    this.name = name;
    this.members = members || [];
    this.projects = projects;
  }
}

module.exports = {
  Role,
  User, 
  Participant,
  Hackathon,
  Project,
  Team,
}