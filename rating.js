const db = require('./dataset');

function teamTaskFittingCoefficient(task, team) {
}

function getTeamStrength(project) {
  const task = project.task;
  const team = project.team;

  /* 
    How good should this team be at this task? Does it fit the task speciality? 
    Or they are trying new technologies, so we don't expect much?

    (Sum of profiled to project role HardSkills of every member) * average(SoftSkills) *
    * teamTaskFittingCoefficient(task, team)
  */

  return 1;
}

function averageTeamsStrength(projects) {
  let strengthSum = 0;

  projects.forEach(project => {
    strengthSum += getTeamStrength(project)
  });

  return strengthSum / projects.length;
}

function projectSuccessCoefficient(project) {
  const task = db.tasks.find(task => {
    return task.projects.indexOf(project) !== -1;
  });
  console.log(task);
  const team = project.team;
  const place = project.place;

  const otherProjects = task.projects;
  const averageOpponentStrength = averageTeamsStrength(otherProjects);

  const teamStrength = getTeamStrength(project);

  let competitionCoefficient = averageOpponentStrength / teamStrength;
  // This coefficient may take into account how many teams project surpassed?
  // How good were these teams? Was the hackathon really challenging? Or was it easy one?

  // Only allow increasing rating grow if weaker teams win
  // Do not reduce received rating if strong team wins
  // Simplest possible calculation
  if (competitionCoefficient > 3) {
    competitionCoefficient = 3;
  } else if (competitionCoefficient < 0.6) {
    competitionCoefficient = 0.6;
  }

  return competitionCoefficient;
}

function impliedEffortCoefficient(projectPartPercentage) {
  const magicCoefficientValue = 0.75;
  // Between 0.25 at zero effort, 2.25 at 100% effort
  return Math.pow(magicCoefficientValue + projectPartPercentage, 2);
}

function hardSkillBase(user) {
  let hs = 0;

  const userParticipations = db.participations.filter(participation => {
    return participation.user = user;
  });


  userParticipations.forEach(participation => {
    /* Get role HS coefficient, get role Implied Effort * projectSuccessCoefficient */
    const userProjectRoles = participation.roles;
    userProjectRoles.forEach(role => {
      const roleHSC = role.hsc;

      // Roles effort percentage can't be less than 10% just for fun
      const rolePercentage = participation.project.effortPartition[role.name] || 0.1;
      //const rolePercentage = participation.rolesProjectImpact[role.name] || 0.1;

      const roleImpliedEffort = impliedEffortCoefficient(rolePercentage);

      const roleHardSkillGain = roleHSC * roleImpliedEffort * projectSuccessCoefficient(participation.project);
      hs += roleHardSkillGain;
    })
  });

  return hs;
}

function hackathonPower(hackathon) {
  /*
    Returns hackathon power or awesomety
    Counted from amount of participants and teams, winning prize
  */
}

/* SOFT SKILL HELPERS */
function teamCommunicatingDifficulty(project) {
  const team = project.team;
  const teamSize = team.members.length;

  // The most basic counting example
  return teamSize / 3;
}

function SoftSkillsBase (user) {
  const ss = 0;

  const userParticipations = participants.filter(participation => {
    participation.user = user;
  });

  userParticipations.forEach(participation => {
    const userProjectRoles = participation.roles;

    userProjectRoles.forEach(role => {
      roleSSC = role.ssc;
      const rolePercentage = 123213; // Dont remember what should be there watch on paper
    });

  })
}

module.exports = {
  hardSkillBase,
}